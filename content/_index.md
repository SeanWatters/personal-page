---
title: Home
---

# Sean Watters

I am currently a PhD student
in the Department of Computer and Information Sciences at the University of Strathclyde,
where I am a member of the
[Mathematically Structured Programming Group](https://msp.cis.strath.ac.uk/index.html).
Prior to that, I did a BSc in Computer Science, also at Strathclyde.

My PhD is on correct-by-construction model checking for coalgebraic modal logics.
I write a lot of Agda code.
My broad interests are dependently-typed functional programming languages, constructive logic, and type theory.

You can get in touch with me via email at: *sean.watters@strath.ac.uk*
